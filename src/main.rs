use error_iter::ErrorIter as _;
use log::error;
use pixels::{Pixels, SurfaceTexture};
use winit::dpi::LogicalSize;
use winit::event::{Event, WindowEvent};
use winit::event_loop::EventLoop;
use winit::keyboard::KeyCode;
use winit::window::WindowBuilder;
use winit_input_helper::WinitInputHelper;

mod audio;
use audio::*;
mod emulator;
use emulator::*;

const WIDTH: u32 = chip8_lib::interpreter::SCREEN_WIDTH as u32;
const HEIGHT: u32 = chip8_lib::interpreter::SCREEN_HEIGHT as u32;

fn main() -> Result<(), anyhow::Error> {
    env_logger::init();
    let event_loop = EventLoop::new()?;
    let mut input = WinitInputHelper::new();
    let window = {
        let size = LogicalSize::new(WIDTH as f64, HEIGHT as f64);
        WindowBuilder::new()
            .with_title("Hello Pixels")
            .with_inner_size(size)
            .with_min_inner_size(size)
            .build(&event_loop)?
    };

    let mut pixels = {
        let window_size = window.inner_size();
        let surface_texture = SurfaceTexture::new(window_size.width, window_size.height, &window);
        Pixels::new(WIDTH, HEIGHT, surface_texture)?
    };

    let (
        _stream, // we NEED to keep this alive, or it will stop the audio stream
        audio_sender,
    ) = start_audio_stream::<Sin>(
        AudioStreamParameters::builder()
            .volume(0.1)
            .build()
            .unwrap(),
    )?;

    let mut state = EmulatorState::new(audio_sender);

    event_loop.run(move |event, window_tartget| {
        // Draw the current frame
        if matches!(
            event,
            Event::WindowEvent {
                event: WindowEvent::RedrawRequested,
                ..
            }
        ) {
            state.draw(pixels.frame_mut());
            if let Err(err) = pixels.render() {
                log_error("pixels.render", err);
                window_tartget.exit();
                return;
            }
        }

        // Handle input events
        if input.update(&event) {
            // Close events
            if state.program_halted() || input.close_requested() {
                window_tartget.exit();
                return;
            }

            // Chip-8 Key  Keyboard
            // ----------  ---------
            // 1 2 3 C     1 2 3 4
            // 4 5 6 D     q w e r
            // 7 8 9 E     a s d f
            // A 0 B F     z x c v
            for (keycode, chip8_key_code) in [
                (KeyCode::Digit1, chip8_lib::KeyCode::Key1),
                (KeyCode::Digit2, chip8_lib::KeyCode::Key2),
                (KeyCode::Digit3, chip8_lib::KeyCode::Key3),
                (KeyCode::Digit4, chip8_lib::KeyCode::KeyC),
                (KeyCode::KeyQ, chip8_lib::KeyCode::Key4),
                (KeyCode::KeyW, chip8_lib::KeyCode::Key5),
                (KeyCode::KeyE, chip8_lib::KeyCode::Key6),
                (KeyCode::KeyR, chip8_lib::KeyCode::KeyD),
                (KeyCode::KeyA, chip8_lib::KeyCode::Key7),
                (KeyCode::KeyS, chip8_lib::KeyCode::Key8),
                (KeyCode::KeyD, chip8_lib::KeyCode::Key9),
                (KeyCode::KeyF, chip8_lib::KeyCode::KeyE),
                (KeyCode::KeyZ, chip8_lib::KeyCode::KeyA),
                (KeyCode::KeyX, chip8_lib::KeyCode::Key0),
                (KeyCode::KeyC, chip8_lib::KeyCode::KeyB),
                (KeyCode::KeyV, chip8_lib::KeyCode::KeyF),
            ] {
                if input.key_pressed(keycode) {
                    state.press_key(chip8_key_code);
                }
                if input.key_released(keycode) {
                    state.release_key(chip8_key_code);
                }
            }

            // Resize the window
            if let Some(size) = input.window_resized() {
                if let Err(err) = pixels.resize_surface(size.width, size.height) {
                    log_error("pixels.resize_surface", err);
                    window_tartget.exit();
                    return;
                }
            }

            // Update internal state and request a redraw
            state.update();
            window.request_redraw();
        }
    })?;
    Ok(())
}

fn log_error<E: std::error::Error + 'static>(method_name: &str, err: E) {
    error!("{method_name}() failed: {err}");
    for source in err.sources().skip(1) {
        error!("  Caused by: {source}");
    }
}
