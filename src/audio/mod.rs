use cpal::{
    traits::{DeviceTrait, HostTrait, StreamTrait},
    Stream,
};
use derive_builder::Builder;
use log::{error, info};
use std::sync::{mpsc::Sender, Arc, Mutex};

mod wave;
pub(crate) use wave::*;

#[derive(Clone, Copy, Debug)]
enum AudioStreamError {
    NoDefaultOutputDevice,
}

impl std::fmt::Display for AudioStreamError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl std::error::Error for AudioStreamError {}

pub(crate) enum AudioMessage {
    Start,
    Stop,
}

#[derive(Builder, Clone, Debug)]
#[builder(build_fn(validate = "Self::validate"))]
pub(crate) struct AudioStreamParameters {
    /// the frequency of the sound
    #[builder(default = "440.0")]
    frequency: f32,
    /// should be between 0 and 1, modulates how much time the sound takes to ramp up and down in volume
    /// lower easing means a softer volume transition
    /// higher easing means a sharper volume transition
    #[builder(default = "0.01")]
    easing: f32,
    /// the volume of the sound, should be between 0 and 1
    #[builder(default = "0.5")]
    volume: f32,
}

impl Default for AudioStreamParameters {
    fn default() -> Self {
        Self::builder().build().unwrap()
    }
}

impl AudioStreamParameters {
    pub(crate) fn builder() -> AudioStreamParametersBuilder {
        AudioStreamParametersBuilder::default()
    }
}

impl AudioStreamParametersBuilder {
    fn validate(&self) -> Result<(), String> {
        if let Some(easing) = self.easing {
            if !(0.0..=1.0).contains(&easing) {
                return Err("easing should be between 0 and 1".into());
            }
        }
        if let Some(volume) = self.volume {
            if !(0.0..=1.0).contains(&volume) {
                return Err("volume should be between 0 and 1".into());
            }
        }
        Ok(())
    }
}

pub(crate) fn start_audio_stream<W: Wave>(
    parameters: AudioStreamParameters,
) -> Result<(Stream, Sender<AudioMessage>), anyhow::Error> {
    let host = cpal::default_host();
    let device = host
        .default_output_device()
        .ok_or(AudioStreamError::NoDefaultOutputDevice)?;
    info!("selected device \"{}\"", device.name()?);
    let supported_config = device.default_output_config()?;
    let config = supported_config.config();
    let sample_rate = config.sample_rate.0;
    let channels = config.channels as usize;
    info!("sample_rate = {sample_rate}");
    info!("channels = {channels}");

    let should_play = Arc::new(Mutex::new(false));
    let (sx, rx) = std::sync::mpsc::channel::<AudioMessage>();
    {
        let should_play = Arc::clone(&should_play);
        std::thread::spawn(move || loop {
            match rx.recv() {
                Err(_) => {
                    info!("send channel has been closed: shutting off recieving thread");
                    break;
                }
                Ok(message) => match message {
                    AudioMessage::Start => {
                        *should_play.lock().unwrap() = true;
                    }
                    AudioMessage::Stop => {
                        *should_play.lock().unwrap() = false;
                    }
                },
            }
        });
    }

    let mut sample_clock = 0;
    let mut envelope = 0.0;
    let mut next_value = move || {
        // advance clock
        sample_clock += 1;
        sample_clock %= sample_rate;
        // update envelope
        if *should_play.lock().unwrap() {
            // this should result in a curve like -exp(-easing / sample_rate * t)+1
            envelope += -parameters.easing * (envelope - 1.0) * 44100.0 / sample_rate as f32;
            if envelope > 0.999 {
                envelope = 1.0
            }
        } else {
            // this should result in a curve like exp(-easing / sample_rate * t)
            envelope += envelope * -parameters.easing * 44100.0 / sample_rate as f32;
            if envelope < 0.001 {
                envelope = 0.0
            }
        }
        W::eval(sample_clock as f32 / sample_rate as f32 * parameters.frequency * W::PERIOD)
            / W::NORMALIZATION
            * envelope
            * parameters.volume
    };
    let stream = device.build_output_stream(
        &config,
        move |data: &mut [f32], _: &cpal::OutputCallbackInfo| {
            for frame in data.chunks_mut(channels) {
                let value = next_value();
                for sample in frame.iter_mut() {
                    *sample = value;
                }
            }
        },
        move |err| {
            error!("audio error: {err:?}");
        },
        None,
    )?;
    stream.play()?;
    Ok((stream, sx))
}
