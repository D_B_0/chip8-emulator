pub(crate) trait Wave {
    /// the period of the wave
    const PERIOD: f32;
    /// the necessary ammount to divide the wave by such that the absolute value
    /// squared, when integrated over a period of the wave, gives a value of 1
    const NORMALIZATION: f32;

    fn eval(t: f32) -> f32;
}

pub(crate) struct Sin;

impl Wave for Sin {
    const PERIOD: f32 = std::f32::consts::TAU;
    const NORMALIZATION: f32 = std::f32::consts::FRAC_1_SQRT_2;

    fn eval(t: f32) -> f32 {
        t.sin()
    }
}

pub(crate) struct Triangle;

impl Wave for Triangle {
    const PERIOD: f32 = 1.0;
    // 1/sqrt(3)
    const NORMALIZATION: f32 = 0.577_350_26;

    fn eval(t: f32) -> f32 {
        4.0 * (t - (t + 0.5).floor()).abs() - 1.0
    }
}

pub(crate) struct Sawtooth;

impl Wave for Sawtooth {
    const PERIOD: f32 = 1.0;
    // 1/sqrt(3)
    const NORMALIZATION: f32 = 0.577_350_26;

    fn eval(t: f32) -> f32 {
        2.0 * (t - (t + 0.5).floor())
    }
}

pub(crate) struct Square;

impl Wave for Square {
    const PERIOD: f32 = 1.0;
    const NORMALIZATION: f32 = 1.0;

    fn eval(t: f32) -> f32 {
        2.0 * (2.0 * t.floor() - (2.0 * t).floor()) + 1.0
    }
}
