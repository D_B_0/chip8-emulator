use std::{sync::mpsc::Sender, time::Instant};

use chip8_lib::{program, Interpreter};
use log::error;

use crate::{AudioMessage, WIDTH};

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
enum EmulatorStatus {
    Running,
    Halted,
    Crashed,
}

pub(crate) struct EmulatorState {
    interpreter: Interpreter,
    status: EmulatorStatus,
    last_time: Option<Instant>,
    audio_sender: Sender<AudioMessage>,
}

impl EmulatorState {
    pub(crate) fn new(audio_sender: Sender<AudioMessage>) -> Self {
        let mut interpreter = Interpreter::new();
        interpreter
            .load_program(program!(
                // initialize the needed registers
                LD V0 20    // x
                LD V1 10    // y
                LD V9 9     // key 9
                LD V7 7     // key 7
                LD V5 5     // key 5
                LD V8 8     // key 8
                LD VF 0xF   // key F
                LD V4 1     // for subtraction purposes
                // wait for key press
                LD V6 K
                // then load the key's sprite
                LD F V6
                // play a sound for a second
                LD V6 60
                LD ST V6
                // now we start the draw-update loop
                loop:
                CLS
                DRW V0 V1 5
                // is '9' pressed? increase x
                SKNP V9
                ADD V0 1
                // is '8' pressed? increase y
                SKNP V8
                ADD V1 1
                // is '7' pressed? decrease x
                SKNP V7
                SUB V0 V4
                // is '5' pressed? decrease y
                SKNP V5
                SUB V1 V4
                // is 'F' pressed? exit program
                SKNP VF
                EXIT
                JP loop
            ))
            .unwrap();
        Self {
            interpreter,
            status: EmulatorStatus::Running,
            last_time: None,
            audio_sender,
        }
    }

    pub(crate) fn program_halted(&self) -> bool {
        self.status == EmulatorStatus::Halted
    }

    pub(crate) fn update(&mut self) {
        if self.status == EmulatorStatus::Running {
            // timer updating
            let curr_time = Instant::now();
            if let Some(last_time) = self.last_time {
                let intervals_elapsed = (curr_time - last_time).as_millis() / (1000 / 60);
                self.interpreter.decrement_timers(intervals_elapsed.clamp(u8::MIN.into(), u8::MAX.into()) as u8);
            }
            self.last_time = Some(curr_time);

            // should audio play?
            if self
                .audio_sender
                .send(if self.interpreter.st() == 0 {
                    AudioMessage::Stop
                } else {
                    AudioMessage::Start
                })
                .is_err()
            {
                // audio errors are not fatal: log and continue
                error!("audio: could not send update");
            }

            // step the interpreter
            self.status = match self.interpreter.step() {
                Ok(false) => EmulatorStatus::Running,
                Ok(true) => {
                    if self.audio_sender.send(AudioMessage::Stop).is_err() {
                        error!("audio: could not send update");
                    }
                    EmulatorStatus::Halted
                }
                Err(err) => {
                    error!("chip8 program crashed: {err:?}");
                    if self.audio_sender.send(AudioMessage::Stop).is_err() {
                        error!("audio: could not send update");
                    }
                    EmulatorStatus::Crashed
                }
            };
        }
    }

    pub(crate) fn draw(&self, frame: &mut [u8]) {
        for (i, pixel) in frame.chunks_exact_mut(4).enumerate() {
            let x = (i % WIDTH as usize) as u8;
            let y = (i / WIDTH as usize) as u8;

            let rgba = if self.interpreter.screen_get_pixel(x, y) {
                [0xdd, 0xdd, 0xdd, 0xff]
            } else {
                [0x22, 0x22, 0x22, 0xff]
            };

            pixel.copy_from_slice(&rgba);
        }
    }

    pub(crate) fn press_key(&mut self, key_code: chip8_lib::KeyCode) {
        self.interpreter.press_key(key_code)
    }

    pub(crate) fn release_key(&mut self, key_code: chip8_lib::KeyCode) {
        self.interpreter.release_key(key_code)
    }
}
