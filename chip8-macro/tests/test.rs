use chip8_lib::Program;
use chip8_macro::program;

#[test]
fn test() {
    assert_eq!(
        program!(ADD V0 1 LD V3 5 ADD V0 V3),
        Program::from_blob(&[0x70, 1, 0x63, 5, 0x80, 0x34])
    );
}
