extern crate proc_macro;

use std::{collections::HashMap, iter::Peekable};

use proc_macro::{token_stream::IntoIter, TokenStream};

fn peek_reg(input: &mut Peekable<IntoIter>) -> Result<String, String> {
    match input
        .peek()
        .ok_or("No register: could not peek")?
        .to_string()
        .strip_prefix('V')
        .ok_or("No register: did not start with 'V'")?
    {
        "0" => Ok("Register::V0".to_owned()),
        "1" => Ok("Register::V1".to_owned()),
        "2" => Ok("Register::V2".to_owned()),
        "3" => Ok("Register::V3".to_owned()),
        "4" => Ok("Register::V4".to_owned()),
        "5" => Ok("Register::V5".to_owned()),
        "6" => Ok("Register::V6".to_owned()),
        "7" => Ok("Register::V7".to_owned()),
        "8" => Ok("Register::V8".to_owned()),
        "9" => Ok("Register::V9".to_owned()),
        "A" | "a" => Ok("Register::VA".to_owned()),
        "B" | "b" => Ok("Register::VB".to_owned()),
        "C" | "c" => Ok("Register::VC".to_owned()),
        "D" | "d" => Ok("Register::VD".to_owned()),
        "E" | "e" => Ok("Register::VE".to_owned()),
        "F" | "f" => Ok("Register::VF".to_owned()),
        rest => Err(format!("No register: '{rest}' is not a register")),
    }
}

fn get_reg(input: &mut Peekable<IntoIter>) -> Result<String, String> {
    let res = peek_reg(input);
    if res.is_ok() {
        input.next();
    }
    res
}

fn peek_addr(
    input: &mut Peekable<IntoIter>,
    labels: &HashMap<String, u16>,
) -> Result<String, String> {
    let next = input.peek().ok_or("No addr: could not peek")?;
    let string = next.to_string();
    let num = if let Some(byte_offset) = labels.get(&string) {
        // TODO: do not hard code 0x200 (how?)
        0x200 + byte_offset
    } else if string.starts_with("0x") {
        u16::from_str_radix(string.strip_prefix("0x").unwrap(), 16)
            .map_err(|err| format!("No address: {err:?}"))?
    } else {
        string
            .parse::<u16>()
            .map_err(|err| format!("No address: {err:?}"))?
    };
    if num > 0xFFF {
        Err(format!("No address: {num} is too big"))
    } else {
        Ok(format!("Address::new({num}).unwrap()"))
    }
}

fn get_addr(
    input: &mut Peekable<IntoIter>,
    labels: &HashMap<String, u16>,
) -> Result<String, String> {
    let res = peek_addr(input, labels);
    if res.is_ok() {
        input.next();
    }
    res
}

fn peek_byte(input: &mut Peekable<IntoIter>) -> Result<u8, String> {
    let next = input.peek().ok_or("No byte: could not peek")?;
    let string = next.to_string();
    if string.starts_with("0x") {
        u8::from_str_radix(string.strip_prefix("0x").unwrap(), 16)
            .map_err(|err| format!("No byte: {err:?}"))
    } else {
        string
            .parse::<u8>()
            .map_err(|err| format!("No byte: {err:?}"))
    }
}

fn get_byte(input: &mut Peekable<IntoIter>) -> Result<u8, String> {
    let res = peek_byte(input);
    if res.is_ok() {
        input.next();
    }
    res
}

fn peek_nibble(input: &mut Peekable<IntoIter>) -> Result<String, String> {
    let next = input.peek().ok_or("No nibble: could not peek")?;
    let string = next.to_string();
    let num = if string.starts_with("0x") {
        u8::from_str_radix(string.strip_prefix("0x").unwrap(), 16)
            .map_err(|err| format!("No nibble: {err:?}"))
    } else {
        string
            .parse::<u8>()
            .map_err(|err| format!("No nibble: {err:?}"))
    }?;
    if num > 0xF {
        Err(format!("No nibble: {num} is too big"))
    } else {
        Ok(format!("Nibble::new({num}).unwrap()"))
    }
}

fn get_nibble(input: &mut Peekable<IntoIter>) -> Result<String, String> {
    let res = peek_nibble(input);
    if res.is_ok() {
        input.next();
    }
    res
}

#[derive(Debug)]
enum InstructionGood {
    Instruction(String),
    Label,
}

fn get_next_instruction(
    input: &mut Peekable<IntoIter>,
    labels: &mut HashMap<String, u16>,
    instruction_count: u16,
) -> Result<InstructionGood, String> {
    use InstructionGood::*;
    let op = input.next().ok_or("could not call next")?;
    let op_str = op.to_string();
    match op_str.to_uppercase().as_str() {
        "CLS" => Ok(Instruction("Instruction::ClearScreen".to_owned())), // CLS
        "EXIT" => Ok(Instruction("Instruction::Exit".to_owned())),       // EXIT
        "RET" => Ok(Instruction("Instruction::Return".to_owned())),      // RET
        "SYS" => Ok(Instruction(format!(
            "Instruction::Sys({})",
            get_addr(input, labels)?
        ))), // SYS addr
        "JP" => {
            if let Ok(addr) = get_addr(input, labels) {
                // JP addr
                Ok(Instruction(format!("Instruction::Jump({addr})")))
            } else if let Ok(reg) = get_reg(input) {
                // JP V0 addr
                if reg == "Register::V0" {
                    Ok(Instruction(format!(
                        "Instruction::JumpV0({})",
                        get_addr(input, labels)?
                    )))
                } else {
                    Err("invalid JP".to_owned())
                }
            } else {
                Err("invalid JP: neither address or register as argument".to_owned())
            }
        }
        "CALL" => Ok(Instruction(format!(
            "Instruction::Call({})",
            get_addr(input, labels)?
        ))), // CALL addr
        "SE" => {
            let reg1 = get_reg(input)?;
            #[allow(clippy::manual_map)]
            if let Ok(byte) = get_byte(input) {
                // SE Vx byte
                Ok(Instruction(format!(
                    "Instruction::SkipEqualValue({reg1}, {byte})"
                )))
            } else if let Ok(reg2) = get_reg(input) {
                // SE Vx Vy
                Ok(Instruction(format!(
                    "Instruction::SkipEqualRegister({reg1}, {reg2})"
                )))
            } else {
                Err("invalid SE".to_owned())
            }
        }
        "SNE" => {
            let reg1 = get_reg(input)?;
            #[allow(clippy::manual_map)]
            if let Ok(byte) = get_byte(input) {
                // SNE Vx byte
                Ok(Instruction(format!(
                    "Instruction::SkipNotEqualValue({reg1}, {byte})"
                )))
            } else if let Ok(reg2) = get_reg(input) {
                // SNE Vx Vy
                Ok(Instruction(format!(
                    "Instruction::SkipNotEqualRegister({reg1}, {reg2})"
                )))
            } else {
                Err("invalid SNE".to_owned())
            }
        }
        "ADD" => {
            if let Ok(reg1) = get_reg(input) {
                #[allow(clippy::manual_map)]
                if let Ok(reg2) = get_reg(input) {
                    // ADD Vx Vy
                    Ok(Instruction(format!(
                        "Instruction::AddRegister({reg1}, {reg2})"
                    )))
                } else if let Ok(byte) = get_byte(input) {
                    // ADD Vx byte
                    Ok(Instruction(format!(
                        "Instruction::AddValue({reg1}, {byte})"
                    )))
                } else {
                    Err("invalid ADD".to_owned())
                }
            } else if input.peek().ok_or("could not peek")?.to_string() == "I" {
                // ADD I Vx
                #[allow(clippy::manual_map)]
                if let Ok(reg) = get_reg(input) {
                    Ok(Instruction(format!("Instruction::AddIReg({reg})")))
                } else {
                    Err("invalid ADD".to_owned())
                }
            } else {
                Err("invalid ADD".to_owned())
            }
        }
        "LD" => {
            if let Ok(reg1) = get_reg(input) {
                if let Ok(reg2) = get_reg(input) {
                    // LD Vx Vy
                    Ok(Instruction(format!(
                        "Instruction::LoadRegister({reg1}, {reg2})"
                    )))
                } else if let Ok(byte) = get_byte(input) {
                    // LD Vx byte
                    Ok(Instruction(format!(
                        "Instruction::LoadValue({reg1}, {byte})"
                    )))
                } else if input.peek().ok_or("could not peek")?.to_string() == "DT" {
                    // LD Vx DT
                    input.next();
                    Ok(Instruction(format!("Instruction::LoadFromDT({reg1})")))
                } else if input.peek().ok_or("could not peek")?.to_string() == "K" {
                    // LD Vx K
                    input.next();
                    Ok(Instruction(format!("Instruction::LoadFromKey({reg1})")))
                } else if input.peek().ok_or("could not peek")?.to_string() == "[" {
                    // LD Vx [I]
                    input.next();
                    if input.next().ok_or("could not call next")?.to_string() == "I"
                        && input.next().ok_or("could not call next")?.to_string() == "]"
                    {
                        Ok(Instruction(format!("Instruction::LoadRegisters({reg1})")))
                    } else {
                        Err("invalid LD".to_owned())
                    }
                } else {
                    Err("invalid LD".to_owned())
                }
            } else if input.peek().ok_or("could not peek")?.to_string() == "DT" {
                input.next().unwrap();
                // LD DT Vx
                Ok(Instruction(format!(
                    "Instruction::LoadToDT({})",
                    get_reg(input)?
                )))
            } else if input.peek().ok_or("could not peek")?.to_string() == "ST" {
                input.next().unwrap();
                // LD ST Vx
                Ok(Instruction(format!(
                    "Instruction::LoadToST({})",
                    get_reg(input)?
                )))
            } else if input.peek().ok_or("could not peek")?.to_string() == "F" {
                input.next().unwrap();
                // LD F Vx
                Ok(Instruction(format!(
                    "Instruction::LoadDigitSprite({})",
                    get_reg(input)?
                )))
            } else if input.peek().ok_or("could not peek")?.to_string() == "B" {
                input.next().unwrap();
                // LD B Vx
                Ok(Instruction(format!(
                    "Instruction::StoreBCD({})",
                    get_reg(input)?
                )))
            } else if input.peek().ok_or("could not peek")?.to_string() == "I" {
                input.next().unwrap();
                // LD I addr
                Ok(Instruction(format!(
                    "Instruction::LoadI({})",
                    get_addr(input, labels)?
                )))
            } else if input.peek().ok_or("could not peek")?.to_string() == "[" {
                // LD [I] Vx
                input.next();
                if input.next().ok_or("could not call next")?.to_string() == "I"
                    && input.next().ok_or("could not call next")?.to_string() == "]"
                {
                    Ok(Instruction(format!(
                        "Instruction::StoreRegisters({})",
                        get_reg(input)?
                    )))
                } else {
                    Err("invalid LD".to_owned())
                }
            } else {
                Err("invalid LD".to_owned())
            }
        }
        "OR" => Ok(Instruction(format!(
            "Instruction::Or({}, {})",
            get_reg(input)?,
            get_reg(input)?
        ))), // OR Vx Vy
        "AND" => Ok(Instruction(format!(
            "Instruction::And({}, {})",
            get_reg(input)?,
            get_reg(input)?
        ))), // AND Vx Vy
        "XOR" => Ok(Instruction(format!(
            "Instruction::Xor({}, {})",
            get_reg(input)?,
            get_reg(input)?
        ))), // XOR Vx Vy
        "SUB" => Ok(Instruction(format!(
            "Instruction::SubRegister({}, {})",
            get_reg(input)?,
            get_reg(input)?
        ))), // SUB Vx Vy
        "SHR" => Ok(Instruction(format!(
            "Instruction::ShiftRight({}, {})",
            get_reg(input)?,
            get_reg(input)?
        ))), // SHR Vx { Vy}
        "SUBN" => Ok(Instruction(format!(
            "Instruction::SubNegativeRegister({}, {})",
            get_reg(input)?,
            get_reg(input)?
        ))), // SUBN Vx Vy
        "SHL" => Ok(Instruction(format!(
            "Instruction::ShiftLeft({}, {})",
            get_reg(input)?,
            get_reg(input)?
        ))), // SHL Vx { Vy}
        "RND" => Ok(Instruction(format!(
            "Instruction::ShiftLeft({}, {})",
            get_reg(input)?,
            get_byte(input)?
        ))), // RND Vx byte
        "DRW" => Ok(Instruction(format!(
            "Instruction::Draw({}, {}, {})",
            get_reg(input)?,
            get_reg(input)?,
            get_nibble(input)?
        ))), // DRW Vx Vy nibble
        "SKP" => Ok(Instruction(format!(
            "Instruction::SkipKeyPressed({})",
            get_reg(input)?
        ))), // SKP Vx
        "SKNP" => Ok(Instruction(format!(
            "Instruction::SkipKeyNotPressed({})",
            get_reg(input)?
        ))), // SKNP Vx
        label => {
            if input.peek().ok_or("could not peek")?.to_string() == ":" {
                input.next().unwrap();
                labels.insert(label.to_lowercase(), instruction_count * 2);
                Ok(Label)
            } else {
                Err(format!("{label} is not a valid instruction"))
            }
        }
    }
}

#[proc_macro]
pub fn program(input: TokenStream) -> TokenStream {
    let mut stream = "{
use chip8_lib::{Address, Instruction, Nibble, Program, Register};
Program::from_instructions(&["
        .to_owned();
    let mut labels = HashMap::new();
    let mut instruction_count = 0;
    let mut input = input.into_iter().peekable();
    loop {
        if input.peek().is_none() {
            break;
        }
        let res = get_next_instruction(&mut input, &mut labels, instruction_count);
        if let Ok(instruction) = res {
            if let InstructionGood::Instruction(instruction) = instruction {
                stream.push_str(&format!("{instruction},"));
                instruction_count += 1;
            }
        } else {
            panic!("could not parse instruction: {}", res.unwrap_err())
        }
    }
    let rest = input.collect::<TokenStream>().to_string();
    if !rest.is_empty() {
        panic!("could not parse all instructions, rest of stream: {rest}");
    }
    stream.push_str("])}");
    stream.parse().unwrap()
}
