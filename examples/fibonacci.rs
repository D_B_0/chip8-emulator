use chip8_lib::{program, Interpreter, Register};

fn fib(n: u8) -> u8 {
    if n == 1 {
        1
    } else if n == 2 {
        2
    } else {
        fib(n - 1) + fib(n - 2)
    }
}

fn main() {
    let mut int = Interpreter::new();
    int.load_program(program!(
        LD V0 0
        LD V1 1
        loop:
        ADD V2 V0
        ADD V2 V1
        LD V0 V1
        LD V1 V2
        LD V2 0
        ADD V3 1
        SE V3 10
        JP loop
        EXIT
    ))
    .unwrap();
    int.run().unwrap();
    println!("V0: 0x{:x}", int.gpr(Register::V0));
    println!(
        "V1: 0x{:x} (should be 0x{:x})",
        int.gpr(Register::V1),
        fib(10)
    );
    println!("V2: 0x{:x}", int.gpr(Register::V2));
    println!("V3: 0x{:x}", int.gpr(Register::V3));
}
