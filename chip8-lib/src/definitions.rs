#[derive(Debug)]
pub enum OperationError {
    Overflow,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u8)]
pub enum Register {
    V0 = 0x0,
    V1 = 0x1,
    V2 = 0x2,
    V3 = 0x3,
    V4 = 0x4,
    V5 = 0x5,
    V6 = 0x6,
    V7 = 0x7,
    V8 = 0x8,
    V9 = 0x9,
    VA = 0xA,
    VB = 0xB,
    VC = 0xC,
    VD = 0xD,
    VE = 0xE,
    VF = 0xF,
}

impl Register {
    pub fn new_panic(value: u8) -> Register {
        value.try_into().unwrap()
    }
}

impl TryFrom<u8> for Register {
    type Error = ();

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0x0 => Ok(Register::V0),
            0x1 => Ok(Register::V1),
            0x2 => Ok(Register::V2),
            0x3 => Ok(Register::V3),
            0x4 => Ok(Register::V4),
            0x5 => Ok(Register::V5),
            0x6 => Ok(Register::V6),
            0x7 => Ok(Register::V7),
            0x8 => Ok(Register::V8),
            0x9 => Ok(Register::V9),
            0xA => Ok(Register::VA),
            0xB => Ok(Register::VB),
            0xC => Ok(Register::VC),
            0xD => Ok(Register::VD),
            0xE => Ok(Register::VE),
            0xF => Ok(Register::VF),
            _ => Err(()),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Nibble(u8);

impl Nibble {
    pub fn new(value: u8) -> Option<Nibble> {
        if value <= 0xF {
            Some(Nibble(value))
        } else {
            None
        }
    }

    /// #### Safety
    ///
    /// The caller should guarantee that `value <= 0xF`
    pub unsafe fn new_unchecked(value: u8) -> Nibble {
        Nibble(value)
    }
}

impl TryFrom<u8> for Nibble {
    type Error = ();

    fn try_from(value: u8) -> Result<Self, ()> {
        Nibble::new(value).ok_or(())
    }
}

impl From<Nibble> for u8 {
    fn from(val: Nibble) -> Self {
        val.0
    }
}

impl From<&Nibble> for u8 {
    fn from(val: &Nibble) -> Self {
        val.0
    }
}

pub type Byte = u8;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Address(u16);

impl Address {
    pub fn new(value: u16) -> Option<Address> {
        if value <= 0xFFF {
            Some(Address(value))
        } else {
            None
        }
    }

    /// #### Safety
    ///
    /// The caller should guarantee that `value <= 0xFFF`
    pub unsafe fn new_unchecked(value: u16) -> Address {
        Address(value)
    }

    pub fn add_inline(&mut self, other: u16) -> Result<(), OperationError> {
        if self.0 + other > 0xFFF {
            Err(OperationError::Overflow)
        } else {
            self.0 += other;
            Ok(())
        }
    }

    pub fn add(&mut self, other: u16) -> Result<Address, OperationError> {
        Address::new(self.0 + other).ok_or(OperationError::Overflow)
    }

    pub(crate) fn from_nibbles(nibble_1: Nibble, nibble_2: Nibble, nibble_3: Nibble) -> Address {
        Address((nibble_1.0 as u16) << 8 | (nibble_2.0 as u16) << 4 | nibble_3.0 as u16)
    }
}

impl From<u8> for Address {
    fn from(value: u8) -> Self {
        Address(value.into())
    }
}

impl TryFrom<u16> for Address {
    type Error = ();

    fn try_from(value: u16) -> Result<Self, ()> {
        Address::new(value).ok_or(())
    }
}

impl From<Address> for usize {
    fn from(val: Address) -> Self {
        val.0 as usize
    }
}

impl From<Address> for u16 {
    fn from(val: Address) -> Self {
        val.0
    }
}

impl From<&Address> for usize {
    fn from(val: &Address) -> Self {
        val.0 as usize
    }
}

impl From<&Address> for u16 {
    fn from(val: &Address) -> Self {
        val.0
    }
}
