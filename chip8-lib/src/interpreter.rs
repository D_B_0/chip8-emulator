use crate::*;

pub const SCREEN_WIDTH: usize = 64;

pub const SCREEN_HEIGHT: usize = 32;

pub(crate) const KEYS_AMMOUNT: usize = 16;

pub(crate) const STACK_SIZE: usize = 16;

#[derive(PartialEq, Eq, Debug)]
pub enum InterpreterError {
    StackPushError,
    StackPopError,
    ProgramCounterOverflow,
    IOverflow,
    ProgramTooLong,
}

enum KeyState {
    WaitingToStoreTo(Register),
    Idle,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
#[repr(u8)]
pub enum KeyCode {
    Key0 = 0,
    Key1,
    Key2,
    Key3,
    Key4,
    Key5,
    Key6,
    Key7,
    Key8,
    Key9,
    KeyA,
    KeyB,
    KeyC,
    KeyD,
    KeyE,
    KeyF,
}

pub struct Interpreter {
    pub(crate) memory: Memory,
    pub(crate) gprs: [Byte; 16],
    pub(crate) i: Address,
    pub(crate) dt: Byte,
    pub(crate) st: Byte,
    pub(crate) pc: Address,
    pub(crate) sp: Byte,
    pub(crate) stack: [Address; STACK_SIZE],
    pub(crate) keys: [bool; KEYS_AMMOUNT],
    pub(crate) screen: [bool; SCREEN_HEIGHT * SCREEN_WIDTH],
    key_state: KeyState,
}

impl Interpreter {
    #[allow(clippy::new_without_default)]
    pub fn new() -> Interpreter {
        Interpreter {
            memory: Memory::new(),
            gprs: [0; 16],
            i: Address::new(0).unwrap(),
            dt: 0,
            st: 0,
            pc: Address::new(0x200).unwrap(),
            sp: 0,
            stack: [Address::new(0).unwrap(); STACK_SIZE],
            keys: [false; KEYS_AMMOUNT],
            screen: [false; SCREEN_HEIGHT * SCREEN_WIDTH],
            key_state: KeyState::Idle,
        }
    }

    pub fn gpr(&self, reg: Register) -> &u8 {
        &self.gprs[reg as usize]
    }

    pub fn gpr_mut(&mut self, reg: Register) -> &mut u8 {
        &mut self.gprs[reg as usize]
    }

    pub fn pc(&self) -> u16 {
        self.pc.into()
    }

    pub(crate) fn increase_pc(&mut self) -> Result<(), InterpreterError> {
        self.pc
            .add_inline(2)
            .map_err(|_| InterpreterError::ProgramCounterOverflow)
    }

    pub fn st(&self) -> u8 {
        self.st
    }

    pub fn load_program(&mut self, program: Program) -> Result<(), InterpreterError> {
        self.memory
            .load_data_to_memory(Address::new(0x200).unwrap(), program.get_slice())
            .map_err(|_| InterpreterError::ProgramTooLong)
    }

    pub fn decrement_timers(&mut self, by: u8) {
        self.dt = self.dt.saturating_sub(by);
        self.st = self.st.saturating_sub(by);
    }

    /// returns `true` if the program should stop
    pub fn step(&mut self) -> Result<bool, InterpreterError> {
        match self.key_state {
            KeyState::WaitingToStoreTo(_) => {}
            KeyState::Idle => {
                let high_byte = *self.memory.mem_at(self.pc);
                let low_byte = *self.memory.mem_at(
                    self.pc
                        .add(1)
                        .map_err(|_| InterpreterError::ProgramCounterOverflow)?,
                );
                let inst = Instruction::parse(high_byte, low_byte);
                if self.execute_instruction(inst)? {
                    return Ok(true);
                }
                if !inst.is_jump() {
                    self.increase_pc()?;
                }
            }
        }
        Ok(false)
    }

    pub fn run(&mut self) -> Result<(), InterpreterError> {
        while !self.step()? {
            if matches!(self.key_state, KeyState::WaitingToStoreTo(_)) {
                panic!("encountered a `StoreFromKey` instruction when calling `Interpreter::run`, this has caused an infinite loop, which is an irrecoverable state")
            }
        }
        Ok(())
    }

    pub fn press_key(&mut self, key: KeyCode) {
        if let KeyState::WaitingToStoreTo(reg) = self.key_state {
            *self.gpr_mut(reg) = key as u8;
            self.key_state = KeyState::Idle;
        }
        self.keys[key as usize] = true;
    }

    pub fn release_key(&mut self, key: KeyCode) {
        self.keys[key as usize] = false;
    }

    /// returns `true` if the program should stop
    pub fn execute_instruction(
        &mut self,
        instruction: Instruction,
    ) -> Result<bool, InterpreterError> {
        match instruction {
            Instruction::Sys(_) => {
                // ignored
            }
            Instruction::ClearScreen => self.screen.fill(false),
            Instruction::Exit => return Ok(true),
            Instruction::Return => {
                self.pc = self.stack_pop()?;
            }
            Instruction::Jump(addr) => {
                self.pc = addr;
            }
            Instruction::Call(addr) => {
                self.stack_push(self.pc)?;
                self.pc = addr;
            }
            Instruction::SkipEqualValue(reg, byte) => {
                if *self.gpr(reg) == byte {
                    self.increase_pc()?;
                }
            }
            Instruction::SkipNotEqualValue(reg, byte) => {
                if *self.gpr(reg) != byte {
                    self.increase_pc()?;
                }
            }
            Instruction::SkipEqualRegister(reg1, reg2) => {
                if self.gpr(reg1) == self.gpr(reg2) {
                    self.increase_pc()?;
                }
            }
            Instruction::SkipNotEqualRegister(reg1, reg2) => {
                if self.gpr(reg1) != self.gpr(reg2) {
                    self.increase_pc()?;
                }
            }
            Instruction::LoadValue(reg, byte) => *self.gpr_mut(reg) = byte,
            Instruction::LoadRegister(reg1, reg2) => *self.gpr_mut(reg1) = *self.gpr(reg2),
            Instruction::Or(reg1, reg2) => *self.gpr_mut(reg1) = *self.gpr(reg1) | *self.gpr(reg2),
            Instruction::And(reg1, reg2) => *self.gpr_mut(reg1) = *self.gpr(reg1) & *self.gpr(reg2),
            Instruction::Xor(reg1, reg2) => *self.gpr_mut(reg1) = *self.gpr(reg1) ^ *self.gpr(reg2),
            Instruction::AddValue(reg1, byte) => {
                let (res, overflow) = self.gpr(reg1).overflowing_add(byte);
                *self.gpr_mut(reg1) = res;
                *self.gpr_mut(Register::VF) = if overflow { 1 } else { 0 };
            }
            Instruction::AddRegister(reg1, reg2) => {
                let (res, overflow) = self.gpr(reg1).overflowing_add(*self.gpr(reg2));
                *self.gpr_mut(reg1) = res;
                *self.gpr_mut(Register::VF) = if overflow { 1 } else { 0 };
            }
            Instruction::SubRegister(reg1, reg2) => {
                let (res, overflow) = self.gpr(reg1).overflowing_sub(*self.gpr(reg2));
                *self.gpr_mut(reg1) = res;
                *self.gpr_mut(Register::VF) = if !overflow { 1 } else { 0 };
            }
            Instruction::ShiftRight(reg1, _reg2) => {
                *self.gpr_mut(Register::VF) = if self.gpr(reg1) & 1 > 0 { 1 } else { 0 };
                *self.gpr_mut(reg1) = self.gpr(reg1) >> 1;
            }
            Instruction::SubNegativeRegister(reg1, reg2) => {
                let (res, overflow) = self.gpr(reg2).overflowing_sub(*self.gpr(reg1));
                *self.gpr_mut(reg1) = res;
                *self.gpr_mut(Register::VF) = if !overflow { 1 } else { 0 };
            }
            Instruction::ShiftLeft(reg1, _reg2) => {
                *self.gpr_mut(Register::VF) = if self.gpr(reg1) & 0b10000000 > 0 {
                    1
                } else {
                    0
                };
                *self.gpr_mut(reg1) = self.gpr(reg1) << 1;
            }
            Instruction::LoadI(addr) => {
                self.i = addr;
            }
            Instruction::JumpV0(addr) => {
                self.pc = addr;
                self.pc
                    .add_inline(*self.gpr(Register::V0) as u16)
                    .map_err(|_| InterpreterError::ProgramCounterOverflow)?;
            }
            Instruction::RandomAnd(reg, byte) => *self.gpr_mut(reg) = rand::random::<u8>() & byte,
            Instruction::Draw(reg1, reg2, nibble) => {
                let x = *self.gpr(reg1);
                let y = *self.gpr(reg2);
                let sprite_len = u8::from(nibble);
                let mut read_addr = self.i;
                let mut erased_pixel = false;
                for dy in 0..sprite_len {
                    let sprite_part = *self.memory.mem_at(read_addr);
                    for dx in 0..8 {
                        let screen_idx = (x as usize + dx as usize) % SCREEN_WIDTH
                            + ((y as usize + dy as usize) % SCREEN_HEIGHT) * SCREEN_WIDTH;
                        if self.screen[screen_idx] && sprite_part & (1 << (8 - dx - 1)) != 0 {
                            erased_pixel = true;
                        }
                        self.screen[screen_idx] ^= sprite_part & (1 << (8 - dx - 1)) != 0;
                    }
                    read_addr
                        .add_inline(1)
                        .map_err(|_| InterpreterError::IOverflow)?;
                }
                if erased_pixel {
                    *self.gpr_mut(Register::VF) = 1;
                } else {
                    *self.gpr_mut(Register::VF) = 0;
                }
            }
            Instruction::SkipKeyPressed(reg) => {
                if (*self.gpr(reg) as usize) < KEYS_AMMOUNT && self.keys[*self.gpr(reg) as usize] {
                    self.increase_pc()?;
                }
            }
            Instruction::SkipKeyNotPressed(reg) => {
                if (*self.gpr(reg) as usize) < KEYS_AMMOUNT && !self.keys[*self.gpr(reg) as usize] {
                    self.increase_pc()?;
                }
            }
            Instruction::LoadFromDT(reg) => *self.gpr_mut(reg) = self.dt,
            Instruction::LoadFromKey(reg) => {
                self.key_state = KeyState::WaitingToStoreTo(reg);
            }
            Instruction::LoadToDT(reg) => self.dt = *self.gpr(reg),
            Instruction::LoadToST(reg) => self.st = *self.gpr(reg),
            Instruction::AddIReg(reg) => {
                self.i
                    .add_inline(*self.gpr(reg) as u16)
                    .map_err(|_| InterpreterError::IOverflow)?;
            }
            Instruction::LoadDigitSprite(reg) => {
                if self.gpr(reg) <= &0xF {
                    self.i = Address::new(*self.gpr(reg) as u16 * 5).unwrap();
                }
            }
            Instruction::StoreBCD(reg) => {
                let val = *self.gpr(reg);
                *self.memory.mem_at_mut(self.i) = (val / 100) % 10;
                *self.memory.mem_at_mut(self.i.add(1).unwrap()) = (val / 10) % 10;
                *self.memory.mem_at_mut(self.i.add(2).unwrap()) = val % 10;
            }
            Instruction::StoreRegisters(reg) => {
                for i in 0..=(reg as u8) {
                    *self.memory.mem_at_mut(
                        self.i
                            .add(i as u16)
                            .map_err(|_| InterpreterError::IOverflow)?,
                    ) = *self.gpr(i.try_into().unwrap());
                }
            }
            Instruction::LoadRegisters(reg) => {
                for i in 0..=(reg as u8) {
                    *self.gpr_mut(i.try_into().unwrap()) = *self.memory.mem_at(
                        self.i
                            .add(i as u16)
                            .map_err(|_| InterpreterError::IOverflow)?,
                    );
                }
            }
        }
        Ok(false)
    }

    pub(crate) fn stack_push(&mut self, pc: Address) -> Result<(), InterpreterError> {
        if self.sp as usize == STACK_SIZE {
            Err(InterpreterError::StackPushError)
        } else {
            self.stack[self.sp as usize] = pc;
            self.sp += 1;
            Ok(())
        }
    }

    pub(crate) fn stack_pop(&mut self) -> Result<Address, InterpreterError> {
        if self.sp == 0 {
            Err(InterpreterError::StackPopError)
        } else {
            self.sp -= 1;
            Ok(self.stack[self.sp as usize])
        }
    }

    pub fn screen_get_pixel(&self, x: u8, y: u8) -> bool {
        if y as usize > SCREEN_HEIGHT || x as usize > SCREEN_WIDTH {
            panic!("requested pixel outside screen: ({x}, {y})");
        }
        self.screen[y as usize * SCREEN_WIDTH + x as usize]
    }

    pub(crate) fn _screen_slice(&self, (x_s, y_s): (u8, u8), (x_e, y_e): (u8, u8)) -> &[bool] {
        &self.screen[(y_s as usize * SCREEN_WIDTH + x_s as usize)
            ..(y_e as usize * SCREEN_WIDTH + x_e as usize)]
    }

    pub(crate) fn _print_screen(&self) {
        for y in 0..SCREEN_HEIGHT {
            for x in 0..SCREEN_WIDTH {
                if self.screen[x + y * SCREEN_WIDTH] {
                    print!("#")
                } else {
                    print!("·")
                }
            }
            println!()
        }
    }
}

#[cfg(test)]
pub(crate) mod test {
    use super::*;
    use crate::definitions::Nibble;

    #[test]
    fn it_should_load_values_to_registers() {
        let mut int = Interpreter::new();
        assert_eq!(
            int.execute_instruction(Instruction::LoadValue(Register::V0, 0x2)),
            Ok(false)
        );
        assert_eq!(int.gpr(Register::V0), &0x2);
        assert_eq!(
            int.execute_instruction(Instruction::LoadValue(Register::V4, 0x7)),
            Ok(false)
        );
        assert_eq!(int.gpr(Register::V4), &0x7);
        assert_eq!(
            int.execute_instruction(Instruction::LoadRegister(Register::V4, Register::V0)),
            Ok(false)
        );
        assert_eq!(int.gpr(Register::V4), &0x2);
    }

    #[test]
    fn it_should_do_math() {
        let mut int = Interpreter::new();
        assert_eq!(
            int.execute_instruction(Instruction::LoadValue(Register::V0, 0x2)),
            Ok(false)
        );
        assert_eq!(
            int.execute_instruction(Instruction::LoadValue(Register::V4, 0x7)),
            Ok(false)
        );
        assert_eq!(
            int.execute_instruction(Instruction::AddValue(Register::V0, 0x3)),
            Ok(false)
        );
        assert_eq!(int.gpr(Register::V0), &0x5);
        assert_eq!(int.gpr(Register::VF), &0x0);
        assert_eq!(
            int.execute_instruction(Instruction::AddRegister(Register::V0, Register::V4)),
            Ok(false)
        );
        assert_eq!(int.gpr(Register::V0), &0xC);
        assert_eq!(int.gpr(Register::VF), &0x0);
        assert_eq!(
            int.execute_instruction(Instruction::LoadValue(Register::V4, 0x1)),
            Ok(false)
        );
        assert_eq!(
            int.execute_instruction(Instruction::SubRegister(Register::V0, Register::V4)),
            Ok(false)
        );
        assert_eq!(int.gpr(Register::V0), &0xB);
        assert_eq!(int.gpr(Register::VF), &0x1);
        assert_eq!(
            int.execute_instruction(Instruction::LoadValue(Register::V0, 0x0)),
            Ok(false)
        );
        assert_eq!(
            int.execute_instruction(Instruction::SubRegister(Register::V0, Register::V4)),
            Ok(false)
        );
        assert_eq!(int.gpr(Register::V0), &0xFF);
        assert_eq!(int.gpr(Register::VF), &0x0);
        assert_eq!(
            int.execute_instruction(Instruction::AddRegister(Register::V0, Register::V4)),
            Ok(false)
        );
        assert_eq!(int.gpr(Register::V0), &0x0);
        assert_eq!(int.gpr(Register::VF), &0x1);
        assert_eq!(
            int.execute_instruction(Instruction::LoadValue(Register::V0, 0x6)),
            Ok(false)
        );
        assert_eq!(
            int.execute_instruction(Instruction::LoadValue(Register::V4, 0xA)),
            Ok(false)
        );
        assert_eq!(
            int.execute_instruction(Instruction::SubNegativeRegister(Register::V0, Register::V4)),
            Ok(false)
        );
        assert_eq!(int.gpr(Register::V0), &0x4);
        assert_eq!(int.gpr(Register::VF), &0x1);
    }

    #[test]
    fn it_should_push_and_pop_from_the_stack() {
        let mut int = Interpreter::new();
        assert_eq!(
            int.execute_instruction(Instruction::Return),
            Err(InterpreterError::StackPopError)
        );
        assert_eq!(
            int.execute_instruction(Instruction::Call(Address::new(0x300).unwrap())),
            Ok(false)
        );
        assert_eq!(int.pc, Address::new(0x300).unwrap());
        assert_eq!(int.stack[0], Address::new(0x200).unwrap());
        assert_eq!(int.execute_instruction(Instruction::Return), Ok(false));
        assert_eq!(int.pc, Address::new(0x200).unwrap());
    }

    #[test]
    fn it_should_draw() {
        let mut int = Interpreter::new();

        for i in 0..4 {
            int.i = Address::new(i * 5).unwrap();
            *int.gpr_mut(Register::V1) = i as u8 * 5;

            int.execute_instruction(Instruction::Draw(
                Register::V0,
                Register::V1,
                Nibble::new(5).unwrap(),
            ))
            .unwrap();
        }

        assert_eq!(int.gpr(Register::VF), &0);

        // 0
        assert_eq!(int._screen_slice((0, 0), (4, 0)), [true, true, true, true]);
        assert_eq!(
            int._screen_slice((0, 1), (4, 1)),
            [true, false, false, true]
        );
        assert_eq!(
            int._screen_slice((0, 2), (4, 2)),
            [true, false, false, true]
        );
        assert_eq!(
            int._screen_slice((0, 3), (4, 3)),
            [true, false, false, true]
        );
        assert_eq!(int._screen_slice((0, 4), (4, 4)), [true, true, true, true]);

        // 1
        assert_eq!(
            int._screen_slice((0, 5), (4, 5)),
            [false, false, true, false]
        );
        assert_eq!(
            int._screen_slice((0, 6), (4, 6)),
            [false, true, true, false]
        );
        assert_eq!(
            int._screen_slice((0, 7), (4, 7)),
            [false, false, true, false]
        );
        assert_eq!(
            int._screen_slice((0, 8), (4, 8)),
            [false, false, true, false]
        );
        assert_eq!(int._screen_slice((0, 9), (4, 9)), [false, true, true, true]);

        // 2
        assert_eq!(
            int._screen_slice((0, 10), (4, 10)),
            [true, true, true, true]
        );
        assert_eq!(
            int._screen_slice((0, 11), (4, 11)),
            [false, false, false, true]
        );
        assert_eq!(
            int._screen_slice((0, 12), (4, 12)),
            [true, true, true, true]
        );
        assert_eq!(
            int._screen_slice((0, 13), (4, 13)),
            [true, false, false, false]
        );
        assert_eq!(
            int._screen_slice((0, 14), (4, 14)),
            [true, true, true, true]
        );

        // 3
        assert_eq!(
            int._screen_slice((0, 15), (4, 15)),
            [true, true, true, true]
        );
        assert_eq!(
            int._screen_slice((0, 16), (4, 16)),
            [false, false, false, true]
        );
        assert_eq!(
            int._screen_slice((0, 17), (4, 17)),
            [true, true, true, true]
        );
        assert_eq!(
            int._screen_slice((0, 18), (4, 18)),
            [false, false, false, true]
        );
        assert_eq!(
            int._screen_slice((0, 19), (4, 19)),
            [true, true, true, true]
        );

        int.i = Address::new(0).unwrap();
        *int.gpr_mut(Register::V1) = 0;

        int.execute_instruction(Instruction::Draw(
            Register::V0,
            Register::V1,
            Nibble::new(5).unwrap(),
        ))
        .unwrap();

        int._print_screen();
        assert_eq!(int.gpr(Register::VF), &1);
        assert_eq!(
            int._screen_slice((0, 0), (4, 0)),
            [false, false, false, false]
        );
        assert_eq!(
            int._screen_slice((0, 1), (4, 1)),
            [false, false, false, false]
        );
        assert_eq!(
            int._screen_slice((0, 2), (4, 2)),
            [false, false, false, false]
        );
        assert_eq!(
            int._screen_slice((0, 3), (4, 3)),
            [false, false, false, false]
        );
        assert_eq!(
            int._screen_slice((0, 4), (4, 4)),
            [false, false, false, false]
        );
    }

    fn fib(n: u8) -> u8 {
        if n == 1 {
            1
        } else if n == 2 {
            2
        } else {
            fib(n - 1) + fib(n - 2)
        }
    }

    #[test]
    fn it_should_calculate_fibonacci() {
        let mut int = Interpreter::new();
        let n = 10;
        int.load_program(Program::from_blob(&[
            0x60, 0x00, // LD V0 1
            0x61, 0x01, // LD V1 1
            0x82, 0x04, // ADD V2, V0
            0x82, 0x14, // ADD V2, V1
            0x80, 0x10, // LD V0 V1
            0x81, 0x20, // LD V1 V2
            0x62, 0x00, // LD V2 0
            0x73, 0x01, // ADD V3, 1
            0x33, n, // SEQ V3, n
            0x12, 0x04, // JMP 0x204
            0x00, 0xFD, // EXIT
        ]))
        .unwrap();
        int.run().unwrap();
        assert_eq!(int.gpr(Register::V1), &fib(n));
    }

    #[test]
    fn it_should_store_and_load_registers() {
        let mut int = Interpreter::new();
        int.gprs = [
            0x0, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD,
            0xEE, 0xFF,
        ];
        int.i = Address::new(0x500).unwrap();
        int.execute_instruction(Instruction::StoreRegisters(Register::VF))
            .unwrap();
        assert_eq!(
            int.memory.mem_slice(int.i, int.i.add(0xF + 1).unwrap()),
            &[
                0x0, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD,
                0xEE, 0xFF,
            ]
        );
        int.i = Address::new(0x400).unwrap();
        int.memory
            .load_data_to_memory(int.i, &[0x3, 0x5, 0x1])
            .unwrap();
        int.execute_instruction(Instruction::LoadRegisters(Register::V2))
            .unwrap();
        assert_eq!(
            int.gprs,
            [
                0x3, 0x5, 0x1, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD,
                0xEE, 0xFF,
            ]
        );
    }

    #[test]
    fn it_should_calculate_the_bcd_correctly() {
        let mut int = Interpreter::new();
        *int.gpr_mut(Register::V0) = 123;
        int.i = 0x200u16.try_into().unwrap();
        int.execute_instruction(Instruction::StoreBCD(Register::V0))
            .unwrap();
        assert_eq!(int.memory.mem_at(int.i), &1);
        assert_eq!(int.memory.mem_at(int.i.add(1).unwrap()), &2);
        assert_eq!(int.memory.mem_at(int.i.add(2).unwrap()), &3);
    }
}
