pub mod definitions;
pub mod instruction;
pub mod interpreter;
pub mod memory;
pub mod program;

pub use definitions::{Address, Byte, Nibble, Register};
pub use instruction::Instruction;
pub use interpreter::{Interpreter, KeyCode};
pub use memory::{Memory, MemoryError};
pub use program::Program;

#[cfg(feature = "macro")]
pub use chip8_macro::program;
