use crate::definitions::{Address, Byte, Nibble, Register};

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Instruction {
    // standard op codes
    Sys(Address),
    ClearScreen,
    Return,
    Jump(Address),
    Call(Address),
    SkipEqualValue(Register, Byte),
    SkipNotEqualValue(Register, Byte),
    SkipEqualRegister(Register, Register),
    SkipNotEqualRegister(Register, Register),
    LoadValue(Register, Byte),
    LoadRegister(Register, Register),
    Or(Register, Register),
    And(Register, Register),
    Xor(Register, Register),
    AddValue(Register, Byte),
    AddRegister(Register, Register),
    SubRegister(Register, Register),
    ShiftRight(Register, Register),
    SubNegativeRegister(Register, Register),
    ShiftLeft(Register, Register),
    LoadI(Address),
    JumpV0(Address),
    RandomAnd(Register, Byte),
    Draw(Register, Register, Nibble),
    SkipKeyPressed(Register),
    SkipKeyNotPressed(Register),
    LoadFromDT(Register),
    LoadFromKey(Register),
    LoadToDT(Register),
    LoadToST(Register),
    AddIReg(Register),
    LoadDigitSprite(Register),
    StoreBCD(Register),
    StoreRegisters(Register),
    LoadRegisters(Register),
    // [super chip48](https://github.com/Chromatophore/HP48-Superchip)
    Exit,
}

impl Instruction {
    pub fn parse(high_byte: u8, low_byte: u8) -> Instruction {
        let high_marker = high_byte >> 4;
        let high_data = high_byte & 0xF;
        let low_data = low_byte >> 4;
        let low_marker = low_byte & 0xF;
        match (high_marker, high_data, low_data, low_marker) {
            (0x0, 0x0, 0xE, 0x0) => Instruction::ClearScreen, // CLS
            (0x0, 0x0, 0xF, 0xD) => Instruction::Exit,        // EXIT
            (0x0, 0x0, 0xE, 0xE) => Instruction::Return,      // RET
            (0x0, nibble_1, nibble_2, nibble_3) => Instruction::Sys(Address::from_nibbles(
                unsafe { Nibble::new_unchecked(nibble_1) },
                unsafe { Nibble::new_unchecked(nibble_2) },
                unsafe { Nibble::new_unchecked(nibble_3) },
            )), // SYS addr
            (0x1, nibble_1, nibble_2, nibble_3) => Instruction::Jump(Address::from_nibbles(
                unsafe { Nibble::new_unchecked(nibble_1) },
                unsafe { Nibble::new_unchecked(nibble_2) },
                unsafe { Nibble::new_unchecked(nibble_3) },
            )), // JP addr
            (0x2, nibble_1, nibble_2, nibble_3) => Instruction::Call(Address::from_nibbles(
                unsafe { Nibble::new_unchecked(nibble_1) },
                unsafe { Nibble::new_unchecked(nibble_2) },
                unsafe { Nibble::new_unchecked(nibble_3) },
            )), // CALL addr
            (0x3, x, _, _) => Instruction::SkipEqualValue(Register::new_panic(x), low_byte), // SE Vx, byte
            (0x4, x, _, _) => Instruction::SkipNotEqualValue(Register::new_panic(x), low_byte), // SNE Vx, byte
            (0x5, x, y, 0x0) => {
                Instruction::SkipEqualRegister(Register::new_panic(x), Register::new_panic(y))
            } // SE Vx, Vy
            (0x6, x, _, _) => Instruction::LoadValue(Register::new_panic(x), low_byte), // LD Vx, byte
            (0x7, x, _, _) => Instruction::AddValue(Register::new_panic(x), low_byte), // ADD Vx, byte
            (0x8, x, y, 0x0) => {
                Instruction::LoadRegister(Register::new_panic(x), Register::new_panic(y))
            } // LD Vx, Vy
            (0x8, x, y, 0x1) => Instruction::Or(Register::new_panic(x), Register::new_panic(y)), // OR Vx, Vy
            (0x8, x, y, 0x2) => Instruction::And(Register::new_panic(x), Register::new_panic(y)), // AND Vx, Vy
            (0x8, x, y, 0x3) => Instruction::Xor(Register::new_panic(x), Register::new_panic(y)), // XOR Vx, Vy
            (0x8, x, y, 0x4) => {
                Instruction::AddRegister(Register::new_panic(x), Register::new_panic(y))
            } // ADD Vx, Vy
            (0x8, x, y, 0x5) => {
                Instruction::SubRegister(Register::new_panic(x), Register::new_panic(y))
            } // SUB Vx, Vy
            (0x8, x, y, 0x6) => {
                Instruction::ShiftRight(Register::new_panic(x), Register::new_panic(y))
            } // SHR Vx {, Vy}
            (0x8, x, y, 0x7) => {
                Instruction::SubNegativeRegister(Register::new_panic(x), Register::new_panic(y))
            } // SUBN Vx, Vy
            (0x8, x, y, 0xE) => {
                Instruction::ShiftLeft(Register::new_panic(x), Register::new_panic(y))
            } // SHL Vx {, Vy}
            (0x9, x, y, 0x0) => {
                Instruction::SkipNotEqualRegister(Register::new_panic(x), Register::new_panic(y))
            } // SNE Vx, Vy
            (0xA, nibble_1, nibble_2, nibble_3) => Instruction::LoadI(Address::from_nibbles(
                unsafe { Nibble::new_unchecked(nibble_1) },
                unsafe { Nibble::new_unchecked(nibble_2) },
                unsafe { Nibble::new_unchecked(nibble_3) },
            )), // LD I, addr
            (0xB, nibble_1, nibble_2, nibble_3) => Instruction::JumpV0(Address::from_nibbles(
                unsafe { Nibble::new_unchecked(nibble_1) },
                unsafe { Nibble::new_unchecked(nibble_2) },
                unsafe { Nibble::new_unchecked(nibble_3) },
            )), // JP V0, addr
            (0xC, x, _, _) => Instruction::RandomAnd(Register::new_panic(x), low_byte), // RND Vx, byte
            (0xD, x, y, nibble) => {
                Instruction::Draw(Register::new_panic(x), Register::new_panic(y), unsafe {
                    Nibble::new_unchecked(nibble)
                })
            } // DRW Vx, Vy, nibble
            (0xE, x, 0x9, 0xE) => Instruction::SkipKeyPressed(Register::new_panic(x)),  // SKP Vx
            (0xE, x, 0xA, 0x1) => Instruction::SkipKeyNotPressed(Register::new_panic(x)), // SKNP Vx
            (0xF, x, 0x0, 0x7) => Instruction::LoadFromDT(Register::new_panic(x)),      // LD Vx, DT
            (0xF, x, 0x0, 0xA) => Instruction::LoadFromKey(Register::new_panic(x)),     // LD Vx, K
            (0xF, x, 0x1, 0x5) => Instruction::LoadToDT(Register::new_panic(x)),        // LD DT, Vx
            (0xF, x, 0x1, 0x8) => Instruction::LoadToST(Register::new_panic(x)),        // LD ST, Vx
            (0xF, x, 0x1, 0xE) => Instruction::AddIReg(Register::new_panic(x)),         // ADD I, Vx
            (0xF, x, 0x2, 0x9) => Instruction::LoadDigitSprite(Register::new_panic(x)), // LD F, Vx
            (0xF, x, 0x3, 0x3) => Instruction::StoreBCD(Register::new_panic(x)),        // LD B, Vx
            (0xF, x, 0x5, 0x5) => Instruction::StoreRegisters(Register::new_panic(x)), // LD [I], Vx
            (0xF, x, 0x6, 0x5) => Instruction::LoadRegisters(Register::new_panic(x)),  // LD Vx, [I]
            _ => panic!("aaaaaa"),
        }
    }

    pub(crate) fn is_jump(&self) -> bool {
        matches!(
            self,
            Instruction::Jump(_) | Instruction::Call(_) | Instruction::JumpV0(_)
        )
    }

    pub(crate) fn encode(&self) -> (u8, u8) {
        match self {
            Instruction::Sys(address) => {
                ((u16::from(address) >> 8) as u8, u16::from(address) as u8)
            }
            Instruction::ClearScreen => (0x00, 0xE0),
            Instruction::Return => (0x00, 0xEE),
            Instruction::Jump(address) => (
                0x10 | (u16::from(address) >> 8) as u8,
                u16::from(address) as u8,
            ),
            Instruction::Call(address) => (
                0x20 | (u16::from(address) >> 8) as u8,
                u16::from(address) as u8,
            ),
            Instruction::SkipEqualValue(reg, byte) => (0x30 | *reg as u8, *byte),
            Instruction::SkipNotEqualValue(reg, byte) => (0x40 | *reg as u8, *byte),
            Instruction::SkipEqualRegister(reg1, reg2) => (0x50 | *reg1 as u8, (*reg2 as u8) << 4),
            Instruction::SkipNotEqualRegister(reg1, reg2) => {
                (0x90 | *reg1 as u8, (*reg2 as u8) << 4)
            }
            Instruction::LoadValue(reg, byte) => (0x60 | *reg as u8, *byte),
            Instruction::LoadRegister(reg1, reg2) => (0x80 | *reg1 as u8, (*reg2 as u8) << 4),
            Instruction::Or(reg1, reg2) => (0x80 | *reg1 as u8, 0x01 | (*reg2 as u8) << 4),
            Instruction::And(reg1, reg2) => (0x80 | *reg1 as u8, 0x02 | (*reg2 as u8) << 4),
            Instruction::Xor(reg1, reg2) => (0x80 | *reg1 as u8, 0x03 | (*reg2 as u8) << 4),
            Instruction::AddValue(reg, byte) => (0x70 | *reg as u8, *byte),
            Instruction::AddRegister(reg1, reg2) => (0x80 | *reg1 as u8, 0x04 | (*reg2 as u8) << 4),
            Instruction::SubRegister(reg1, reg2) => (0x80 | *reg1 as u8, 0x05 | (*reg2 as u8) << 4),
            Instruction::ShiftRight(reg1, reg2) => (0x80 | *reg1 as u8, 0x06 | (*reg2 as u8) << 4),
            Instruction::SubNegativeRegister(reg1, reg2) => {
                (0x80 | *reg1 as u8, 0x07 | (*reg2 as u8) << 4)
            }
            Instruction::ShiftLeft(reg1, reg2) => (0x80 | *reg1 as u8, 0x0E | (*reg2 as u8) << 4),
            Instruction::LoadI(addr) => {
                (0xA0 | (u16::from(addr) >> 8) as u8, u16::from(addr) as u8)
            }
            Instruction::JumpV0(addr) => {
                (0xB0 | (u16::from(addr) >> 8) as u8, u16::from(addr) as u8)
            }
            Instruction::RandomAnd(reg, byte) => (0xC0 | *reg as u8, *byte),
            Instruction::Draw(reg1, reg2, nibble) => {
                (0xD0 | *reg1 as u8, (*reg2 as u8) << 4 | u8::from(nibble))
            }
            Instruction::SkipKeyPressed(reg) => (0xE0 | *reg as u8, 0x9E),
            Instruction::SkipKeyNotPressed(reg) => (0xE0 | *reg as u8, 0xA1),
            Instruction::LoadFromDT(reg) => (0xF0 | *reg as u8, 0x07),
            Instruction::LoadFromKey(reg) => (0xF0 | *reg as u8, 0x0A),
            Instruction::LoadToDT(reg) => (0xF0 | *reg as u8, 0x15),
            Instruction::LoadToST(reg) => (0xF0 | *reg as u8, 0x18),
            Instruction::AddIReg(reg) => (0xF0 | *reg as u8, 0x1E),
            Instruction::LoadDigitSprite(reg) => (0xF0 | *reg as u8, 0x29),
            Instruction::StoreBCD(reg) => (0xF0 | *reg as u8, 0x33),
            Instruction::StoreRegisters(reg) => (0xF0 | *reg as u8, 0x55),
            Instruction::LoadRegisters(reg) => (0xF0 | *reg as u8, 0x65),
            Instruction::Exit => (0x00, 0xFD),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn it_should_parse_correctly() {
        assert_eq!(Instruction::parse(0x00, 0xE0), Instruction::ClearScreen);
        assert_eq!(Instruction::parse(0x00, 0xEE), Instruction::Return); // RET
        assert_eq!(
            Instruction::parse(0x01, 0x23),
            Instruction::Sys(unsafe { Address::new_unchecked(0x123) })
        ); // SYS addr
        assert_eq!(
            Instruction::parse(0x11, 0x23),
            Instruction::Jump(unsafe { Address::new_unchecked(0x123) })
        ); // JP addr
        assert_eq!(
            Instruction::parse(0x21, 0x23),
            Instruction::Call(unsafe { Address::new_unchecked(0x123) })
        ); // CALL addr
        assert_eq!(
            Instruction::parse(0x31, 0xFE),
            Instruction::SkipEqualValue(Register::V1, 0xFE)
        ); // SE Vx, byte
        assert_eq!(
            Instruction::parse(0x42, 0x0F),
            Instruction::SkipNotEqualValue(Register::V2, 0x0F)
        ); // SNE Vx, byte
        assert_eq!(
            Instruction::parse(0x56, 0xF0),
            Instruction::SkipEqualRegister(Register::V6, Register::VF)
        ); // SE Vx, Vy
        assert_eq!(
            Instruction::parse(0x63, 0x05),
            Instruction::LoadValue(Register::V3, 0x05)
        ); // LD Vx, byte
        assert_eq!(
            Instruction::parse(0x76, 0x35),
            Instruction::AddValue(Register::V6, 0x35)
        ); // ADD Vx, byte

        assert_eq!(
            Instruction::parse(0x80, 0x10),
            Instruction::LoadRegister(Register::V0, Register::V1)
        ); // LD Vx, Vy
        assert_eq!(
            Instruction::parse(0x80, 0x11),
            Instruction::Or(Register::V0, Register::V1)
        ); // OR Vx, Vy
        assert_eq!(
            Instruction::parse(0x80, 0x12),
            Instruction::And(Register::V0, Register::V1)
        ); // AND Vx, Vy
        assert_eq!(
            Instruction::parse(0x80, 0x13),
            Instruction::Xor(Register::V0, Register::V1)
        ); // XOR Vx, Vy
        assert_eq!(
            Instruction::parse(0x80, 0x14),
            Instruction::AddRegister(Register::V0, Register::V1)
        ); // ADD Vx, Vy
        assert_eq!(
            Instruction::parse(0x80, 0x15),
            Instruction::SubRegister(Register::V0, Register::V1)
        ); // SUB Vx, Vy
        assert_eq!(
            Instruction::parse(0x80, 0x16),
            Instruction::ShiftRight(Register::V0, Register::V1)
        ); // SHR Vx {, Vy}
        assert_eq!(
            Instruction::parse(0x80, 0x17),
            Instruction::SubNegativeRegister(Register::V0, Register::V1)
        ); // SUBN Vx, Vy
        assert_eq!(
            Instruction::parse(0x80, 0x1E),
            Instruction::ShiftLeft(Register::V0, Register::V1)
        ); // SHL Vx {, Vy}
        assert_eq!(
            Instruction::parse(0x90, 0x10),
            Instruction::SkipNotEqualRegister(Register::V0, Register::V1)
        ); // SNE Vx, Vy
        assert_eq!(
            Instruction::parse(0xA2, 0x34),
            Instruction::LoadI(unsafe { Address::new_unchecked(0x234) })
        ); // LD I, addr
        assert_eq!(
            Instruction::parse(0xB4, 0x56),
            Instruction::JumpV0(unsafe { Address::new_unchecked(0x456) })
        ); // JP V0, addr
        assert_eq!(
            Instruction::parse(0xC6, 0x63),
            Instruction::RandomAnd(Register::V6, 0x63)
        ); // RND Vx, byte
        assert_eq!(
            Instruction::parse(0xD8, 0x92),
            Instruction::Draw(Register::V8, Register::V9, unsafe {
                Nibble::new_unchecked(0x2)
            })
        ); // DRW Vx, Vy, nibble
        assert_eq!(
            Instruction::parse(0xEA, 0x9E),
            Instruction::SkipKeyPressed(Register::VA)
        ); // SKP Vx
        assert_eq!(
            Instruction::parse(0xEB, 0xA1),
            Instruction::SkipKeyNotPressed(Register::VB)
        ); // SKNP Vx
        assert_eq!(
            Instruction::parse(0xFC, 0x07),
            Instruction::LoadFromDT(Register::VC)
        ); // LD Vx, DT
        assert_eq!(
            Instruction::parse(0xFD, 0x0A),
            Instruction::LoadFromKey(Register::VD)
        ); // LD Vx, K
        assert_eq!(
            Instruction::parse(0xFE, 0x15),
            Instruction::LoadToDT(Register::VE)
        ); // LD DT, Vx
        assert_eq!(
            Instruction::parse(0xFF, 0x18),
            Instruction::LoadToST(Register::VF)
        ); // LD ST, Vx
        assert_eq!(
            Instruction::parse(0xF0, 0x1E),
            Instruction::AddIReg(Register::V0)
        ); // ADD I, Vx
        assert_eq!(
            Instruction::parse(0xF1, 0x29),
            Instruction::LoadDigitSprite(Register::V1)
        ); // LD F, Vx
        assert_eq!(
            Instruction::parse(0xF2, 0x33),
            Instruction::StoreBCD(Register::V2)
        ); // LD B, Vx
        assert_eq!(
            Instruction::parse(0xF3, 0x55),
            Instruction::StoreRegisters(Register::V3)
        ); // LD [I], Vx
        assert_eq!(
            Instruction::parse(0xF4, 0x65),
            Instruction::LoadRegisters(Register::V4)
        ); // LD Vx, [I]
    }
}
