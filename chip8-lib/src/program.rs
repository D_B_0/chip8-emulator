use crate::Instruction;

#[derive(Default, PartialEq, Debug)]
pub struct Program {
    program: Vec<u8>,
}

impl Program {
    pub fn empty() -> Program {
        Program::default()
    }

    pub fn from_blob(blob: &[u8]) -> Program {
        Program {
            program: blob.to_vec(),
        }
    }

    pub fn from_instructions(vec: &[Instruction]) -> Program {
        Program {
            program: vec
                .iter()
                .map(Instruction::encode)
                .flat_map(|(high_byte, low_byte)| [high_byte, low_byte])
                .collect(),
        }
    }

    pub(crate) fn get_slice(&self) -> &[u8] {
        &self.program[..]
    }

    pub fn push_instruction(&mut self, instruction: Instruction) -> &mut Self {
        let (high_byte, low_byte) = instruction.encode();
        self.program.push(high_byte);
        self.program.push(low_byte);
        self
    }

    pub fn get_offset_to_last_instruction(&self) -> Option<u16> {
        if self.program.is_empty() {
            None
        } else {
            u16::try_from(self.program.len())
                .ok()
                .map(|offset| offset - 2)
        }
    }
}

#[cfg(test)]
mod test {
    use crate::{Address, Register};

    use super::*;

    #[test]
    fn it_should_encode_correctly() {
        use Instruction::*;
        use Register::*;
        let n = 10;
        let mut prog = Program::empty();
        prog.push_instruction(LoadValue(V0, 0))
            .push_instruction(LoadValue(V1, 1))
            .push_instruction(AddRegister(V2, V0));
        let label_1 = prog.get_offset_to_last_instruction().unwrap();
        prog.push_instruction(AddRegister(V2, V1))
            .push_instruction(LoadRegister(V0, V1))
            .push_instruction(LoadRegister(V1, V2))
            .push_instruction(LoadValue(V2, 0))
            .push_instruction(AddValue(V3, 1))
            .push_instruction(SkipEqualValue(V3, n))
            .push_instruction(Jump(Address::new(0x200).unwrap().add(label_1).unwrap()))
            .push_instruction(Exit);
        assert_eq!(
            prog.program,
            [
                0x60, 0x00, // LD V0 1
                0x61, 0x01, // LD V1 1
                0x82, 0x04, // ADD V2, V0
                0x82, 0x14, // ADD V2, V1
                0x80, 0x10, // LD V0 V1
                0x81, 0x20, // LD V1 V2
                0x62, 0x00, // LD V2 0
                0x73, 0x01, // ADD V3, 1
                0x33, n, // SEQ V3, n
                0x12, 0x04, // JMP 0x204
                0x00, 0xFD, // EXIT
            ]
        );
    }
}
