use crate::definitions::Address;

#[derive(Debug, PartialEq, Eq)]
pub enum MemoryError {
    IndexOutOfBounds,
    DataTooLong,
}

/// |name |ammt|size (u8)|offset|
/// |     |    |         |      |
/// |gprs |  16|        1|     0|
/// |i    |   1|        2|    16|
/// |dt   |   1|        1|    18|
/// |st   |   1|        1|    19|
/// |pc   |   1|        2|    20|
/// |sp   |   1|        1|    22|
/// |stack|  16|        2|    23|
///
/// total: 440 bits
pub struct Memory {
    mem: [u8; 4096],
}

impl Memory {
    #[allow(clippy::new_without_default)]
    pub fn new() -> Memory {
        let mut mem = Memory { mem: [0; 4096] };
        mem.load_data_to_memory(0u16.try_into().unwrap(), &[0xF0, 0x90, 0x90, 0x90, 0xF0])
            .unwrap();
        mem.load_data_to_memory(5u16.try_into().unwrap(), &[0x20, 0x60, 0x20, 0x20, 0x70])
            .unwrap();
        mem.load_data_to_memory(10u16.try_into().unwrap(), &[0xF0, 0x10, 0xF0, 0x80, 0xF0])
            .unwrap();
        mem.load_data_to_memory(15u16.try_into().unwrap(), &[0xF0, 0x10, 0xF0, 0x10, 0xF0])
            .unwrap();
        mem.load_data_to_memory(20u16.try_into().unwrap(), &[0x90, 0x90, 0xF0, 0x10, 0x10])
            .unwrap();
        mem.load_data_to_memory(25u16.try_into().unwrap(), &[0xF0, 0x80, 0xF0, 0x10, 0xF0])
            .unwrap();
        mem.load_data_to_memory(30u16.try_into().unwrap(), &[0xF0, 0x80, 0xF0, 0x90, 0xF0])
            .unwrap();
        mem.load_data_to_memory(35u16.try_into().unwrap(), &[0xF0, 0x10, 0x20, 0x40, 0x40])
            .unwrap();
        mem.load_data_to_memory(40u16.try_into().unwrap(), &[0xF0, 0x90, 0xF0, 0x90, 0xF0])
            .unwrap();
        mem.load_data_to_memory(45u16.try_into().unwrap(), &[0xF0, 0x90, 0xF0, 0x10, 0xF0])
            .unwrap();
        mem.load_data_to_memory(50u16.try_into().unwrap(), &[0xF0, 0x90, 0xF0, 0x90, 0x90])
            .unwrap();
        mem.load_data_to_memory(55u16.try_into().unwrap(), &[0xE0, 0x90, 0xE0, 0x90, 0xE0])
            .unwrap();
        mem.load_data_to_memory(60u16.try_into().unwrap(), &[0xF0, 0x80, 0x80, 0x80, 0xF0])
            .unwrap();
        mem.load_data_to_memory(65u16.try_into().unwrap(), &[0xE0, 0x90, 0x90, 0x90, 0xE0])
            .unwrap();
        mem.load_data_to_memory(70u16.try_into().unwrap(), &[0xF0, 0x80, 0xF0, 0x80, 0xF0])
            .unwrap();
        mem.load_data_to_memory(75u16.try_into().unwrap(), &[0xF0, 0x80, 0xF0, 0x80, 0x80])
            .unwrap();

        mem
    }

    pub(crate) fn load_data_to_memory(
        &mut self,
        address: Address,
        data: &[u8],
    ) -> Result<(), MemoryError> {
        if data.len() + usize::from(address) > 4095 {
            Err(MemoryError::DataTooLong)
        } else {
            for (offset, data) in data.iter().enumerate() {
                self.mem[usize::from(address) + offset] = *data
            }
            Ok(())
        }
    }

    #[allow(dead_code)]
    fn get_u16(&self, address: Address) -> Result<u16, MemoryError> {
        if u16::from(address) >= 4094 {
            Err(MemoryError::IndexOutOfBounds)
        } else {
            Ok(((self.mem[usize::from(address) + 1] as u16) << 8)
                | self.mem[usize::from(address)] as u16)
        }
    }

    #[allow(dead_code)]
    fn set_u16(&mut self, address: Address, value: u16) -> Result<(), MemoryError> {
        if u16::from(address) >= 4094 {
            return Err(MemoryError::IndexOutOfBounds);
        }
        self.mem[usize::from(address) + 1] = (value >> 8) as u8;
        self.mem[usize::from(address)] = value as u8;
        Ok(())
    }

    pub fn mem_at(&self, idx: Address) -> &u8 {
        &self.mem[usize::from(idx)]
    }

    pub fn mem_at_mut(&mut self, idx: Address) -> &mut u8 {
        &mut self.mem[usize::from(idx)]
    }

    pub fn mem_slice(&self, start: Address, end: Address) -> &[u8] {
        &self.mem[usize::from(start)..usize::from(end)]
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_allows_valid_u16_reads_and_writes() {
        let mut memory = Memory::new();
        assert!(memory
            .set_u16(unsafe { Address::new_unchecked(100) }, 300)
            .is_ok());
        assert_eq!(
            memory.get_u16(unsafe { Address::new_unchecked(100) }),
            Ok(300)
        );
    }
}
